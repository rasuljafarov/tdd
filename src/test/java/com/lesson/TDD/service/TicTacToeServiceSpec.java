package com.lesson.TDD.service;


import com.lesson.TDD.dto.GameStepDto;
import com.lesson.TDD.mapper.TicTacToeMapperImpl;
import com.lesson.TDD.model.GameStep;
import com.lesson.TDD.repository.TicTacToeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TicTacToeServiceSpec {

    @Mock
    private TicTacToeRepository ticTacToeRepository;

    @InjectMocks
    private TicTacToeService ticTacToeService;

    @Captor
    private ArgumentCaptor<Long> longArgumentCaptor;

    @Spy
    private TicTacToeMapperImpl ticTacToeMapperImpl; //not good way

    private GameStepDto gameStepDto;

    @BeforeEach
    public void init() {
//        ticTacToeMapper = TicTacToeMapper.INSTANCE;
        gameStepDto = new GameStepDto(1, 1, 'x');
    }

    @Test
    public void whenXOutsideBoardThenRuntimeException() {
        assertThatThrownBy(() -> ticTacToeService.play(new GameStepDto(0, 2, 'x')))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("x is outside of board");
    }

    @Test
    public void whenYOutsideBoardThenRuntimeException() {
        assertThatThrownBy(() -> ticTacToeService.play(new GameStepDto(1, 4, 'o')))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("y is outside of board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        GameStepDto gameStepDto2 = new GameStepDto(1, 1, 'o');
        ticTacToeService.play(gameStepDto);
        assertThatThrownBy(() -> ticTacToeService.play(gameStepDto2))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    public void whenPlayThenNoWinner() {
        String actual = ticTacToeService.play(gameStepDto);
        assertEquals("no winner", actual);
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        ticTacToeService.play(gameStepDto); //X
        ticTacToeService.play(new GameStepDto(1, 2, 'o')); // O
        ticTacToeService.play(new GameStepDto(2, 1, 'x')); // X
        ticTacToeService.play(new GameStepDto(2, 2, 'o')); // O
        String actual = ticTacToeService.play(new GameStepDto(3, 1, 'x')); // X
        assertEquals("x is the winner", actual);
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinnerForO() {
        ticTacToeService.play(gameStepDto); //X
        ticTacToeService.play(new GameStepDto(1, 2, 'o')); // O
        ticTacToeService.play(new GameStepDto(2, 1, 'x')); // X
        ticTacToeService.play(new GameStepDto(2, 2, 'o')); // O
        ticTacToeService.play(new GameStepDto(2, 3, 'x')); // x
        String actual = ticTacToeService.play(new GameStepDto(3, 2, 'o')); // o
        assertEquals("o is the winner", actual);
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        ticTacToeService.play(new GameStepDto(2, 1, 'x')); // X
        ticTacToeService.play(new GameStepDto(1, 1, 'o')); // O
        ticTacToeService.play(new GameStepDto(3, 1, 'x')); // X
        ticTacToeService.play(new GameStepDto(1, 2, 'o')); // O
        ticTacToeService.play(new GameStepDto(2, 2, 'x')); // X
        String actual = ticTacToeService.play(new GameStepDto(1, 3, 'o')); // O
        assertEquals("o is the winner", actual);
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToeService.play(new GameStepDto(1, 1, 'x')); // X
        ticTacToeService.play(new GameStepDto(1, 2, 'o')); // O
        ticTacToeService.play(new GameStepDto(2, 2, 'x')); // X
        ticTacToeService.play(new GameStepDto(1, 3, 'o')); // O
        String actual = ticTacToeService.play(new GameStepDto(3, 3, 'x')); // x
        assertEquals("x is the winner", actual);
    }

    @Test
    public void whenPlayAndRightTopBottomDiagonalLineThenWinner() {
        ticTacToeService.play(new GameStepDto(1, 1, 'x')); // X
        ticTacToeService.play(new GameStepDto(1, 3, 'o')); // O
        ticTacToeService.play(new GameStepDto(2, 1, 'x')); // X
        ticTacToeService.play(new GameStepDto(2, 2, 'o')); // O
        ticTacToeService.play(new GameStepDto(2, 3, 'x')); // X
        String actual = ticTacToeService.play(new GameStepDto(3, 1, 'o')); // O
        assertEquals("o is the winner", actual);
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToeService.play(new GameStepDto(1, 1, 'x')); //x
        ticTacToeService.play(new GameStepDto(1, 2, 'o')); //o
        ticTacToeService.play(new GameStepDto(1, 3, 'x')); //x
        ticTacToeService.play(new GameStepDto(2, 1, 'o')); //o
        ticTacToeService.play(new GameStepDto(2, 3, 'x')); //x
        ticTacToeService.play(new GameStepDto(2, 2, 'o')); //o
        ticTacToeService.play(new GameStepDto(3, 1, 'x')); //x
        ticTacToeService.play(new GameStepDto(3, 3, 'o')); //o
        String actual = ticTacToeService.play(new GameStepDto(3, 2, 'x')); //x
        assertEquals("The result is draw", actual);
    }

    @Test
    public void saveGameStepOnTheBoard() {
        String actual = ticTacToeService.play(gameStepDto);
        GameStep gameStep = ticTacToeMapperImpl.toGameStep(gameStepDto);
        verify(ticTacToeRepository, times(1)).save(gameStep);
        assertEquals("no winner", actual);
    }

    @Test
    public void shouldFindOneGameStep() {
        //given
        when(ticTacToeRepository.findById(1L))
                .thenReturn(Optional.of(new GameStep(1, 1, 1, 'x')));

        //when
        GameStepDto gameStepDto = ticTacToeService.findGameStepById(1L);

        //then
        verify(ticTacToeRepository, times(1)).findById(longArgumentCaptor.capture());
        assertEquals(longArgumentCaptor.getValue(), 1L);
        assertEquals(gameStepDto, new GameStepDto(1, 1, 'x'));
    }

    @Test
    public void shouldReturnEmptyGameStep() {
        //given
        when(ticTacToeRepository.findById(0L)).thenReturn(Optional.empty());

        //when
        GameStepDto gameStepDto = ticTacToeService.findGameStepById(0L);

        //then
        verify(ticTacToeRepository, times(1)).findById(longArgumentCaptor.capture());
        assertEquals(longArgumentCaptor.getValue(), 0L);
        assertEquals(gameStepDto, new GameStepDto());
    }
}
