package com.lesson.TDD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "game_step")
public class GameStep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int xAxis;
    private int yAxis;
    private Character player;

}
