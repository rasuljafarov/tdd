package com.lesson.TDD.controller;

import com.lesson.TDD.dto.GameStepDto;
import com.lesson.TDD.service.TicTacToeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/play")
@RequiredArgsConstructor
public class TicTacToeController {

    private final TicTacToeService ticTacToeService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public String play(@RequestBody GameStepDto gameStepDto) {
        return ticTacToeService.play(gameStepDto);
    }

    @GetMapping("/{id}")
    public GameStepDto findGameStepById(@PathVariable Long id){
     return ticTacToeService.findGameStepById(id);
    }

}
