package com.lesson.TDD.service;

import com.lesson.TDD.dto.GameStepDto;
import com.lesson.TDD.mapper.TicTacToeMapper;
import com.lesson.TDD.model.GameStep;
import com.lesson.TDD.repository.TicTacToeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class TicTacToeService {

    private final TicTacToeRepository ticTacToeRepository;
    private final TicTacToeMapper ticTacToeMapper = TicTacToeMapper.INSTANCE;

    char lastPlayer = '_';
    int playCount = 0;
    private static final int SIZE = 3;

    char[][] board = {
            {'_', '_', '_'},
            {'_', '_', '_'},
            {'_', '_', '_'}
    };

    public String play(GameStepDto dto) {
        ticTacToeRepository.save(ticTacToeMapper.toGameStep(dto));
        int x = dto.getXAxis();
        int y = dto.getYAxis();
        lastPlayer = dto.getPlayer();
        checkAxes(x, "x");
        checkAxes(y, "y");
        setBox(x, y);
        ++playCount;
        if (isWin(x, y)) {
            return lastPlayer + " is the winner";
        } else if (isDraw(playCount)) {
            return "The result is draw";
        } else {
            return "no winner";
        }
    }

    private boolean isWin(int x, int y) {
        int playerTotal = lastPlayer * SIZE;
        int[] sum = new int[4];
        for (int i = 0; i < SIZE; i++) {
            sum[0] += board[i][i];
            sum[1] += board[i][SIZE - i - 1];
            sum[2] += board[i][y - 1];
            sum[3] += board[x - 1][i];
        }
        return Arrays.stream(sum).anyMatch(s -> s == playerTotal);
    }

    public boolean isDraw(int playCount) {
        return playCount == SIZE * SIZE;
    }

    private void checkAxes(int n, String axis) {
        if (n < 1 || n > 3) {
            throw new RuntimeException(axis + " is outside of board");
        }
    }

    private void setBox(int x, int y) {
        if (board[x - 1][y - 1] != '_') {
            throw new RuntimeException("Box is occupied");
        } else {
            board[x - 1][y - 1] = lastPlayer;
        }
    }

    public GameStepDto findGameStepById(Long id) {
        GameStep gameStep = ticTacToeRepository.findById(id).orElse(new GameStep());
        return ticTacToeMapper.toGameStepDto(gameStep);
    }
}
