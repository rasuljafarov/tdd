package com.lesson.TDD.repository;

import com.lesson.TDD.model.GameStep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicTacToeRepository extends JpaRepository<GameStep, Long> {
}
