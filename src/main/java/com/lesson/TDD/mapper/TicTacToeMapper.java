package com.lesson.TDD.mapper;

import com.lesson.TDD.dto.GameStepDto;
import com.lesson.TDD.model.GameStep;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TicTacToeMapper {

    TicTacToeMapper INSTANCE = Mappers.getMapper(TicTacToeMapper.class);

    GameStep toGameStep(GameStepDto gameStepDto);

    GameStepDto toGameStepDto(GameStep gameStep);
}
